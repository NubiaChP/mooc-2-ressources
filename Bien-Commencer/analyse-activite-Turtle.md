Objectifs  
    Faire découvrir la définition de python
    Se familiariser avec les notions de base du langage python
    Explorer les différentes commandes de base du langage python
    Identifier l'usage de chacune des commandes
    Tracer les figures proposées sur la feuille de travail
    Créer un script qui dessinera les figures
    Expliquer ce que chaque commande fait

Pré-requi à cette activité
    Avoir des connaissances en anglais
    Avoir des connaissances en géometrie
    Savoir lire et bien interpréter les dindications

Durée de l'activité
    6 h de travail

Niveau
    Classe de seconde (débutant)

Exercices cibles
    Pratiquer les 10 premiers figures avec différentes pixils

Description du déroulement de l'activité
    Élaboration d'un brainstorming sur ce que veut dire python?
    Rassemblement des idées et définition de python
    Exploration sur ordinateur les différentes commandes que python propose 
    Création des traces selon les envies des élèves
    Élaboration des figures proposées sur la feuille de travail
    Ne pas avancer jusque la figure soit réalisé comme indiqué sur la feuille de travail
    Ne pas hésiter à demander de l'aide si jamais il y a des blocages

Anticipation des difficultés des élèves
    Appropriation des mouvements des commandes
    Difficulté à continuer l'activité s'il/elle ne trouve pas une solution

Gestion de l'hétérogénéité
    Passer plus de temps avec le/les élève/s qui a/ont/ le plus besoin
    Ceux qui avances plus rapidement auront la chance d'aider les autres qui ont plus des difficultés
    Préparaer une diversité d'exercices de difficulté moins élevée pour ceux qui ont plus de mal et qui voudraient 'y      exercer le plus
    Proposer des explication et mises en pratique en dehors du cours

    
